<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
  <form action="/welcome" method=post>  
    @csrf
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <label for="nama1">First Name</label><br><br> 
    <input type="text" name="nama1"><br><br>
    <label for="nama2">Last Name</label><br><br>
    <input type="text" name="nama2"><br><br>
    <label>Gender</label><br><br>
    <input type="radio" name="gender"> Male <br>
    <input type="radio" name="gender"> Female <br>
    <input type="radio" name="gender"> Other <br><br>
    <label>Nationality</label><br><br>
    <select>
        <option>Indonesian</option>
        <option>Singaporean</option>
        <option>Malaysian</option>
        <option>Australian</option>
    </select>
    <br><br>
    <label>Bio :</label><br><br>
    <textarea cols="40" rows="10"></textarea>
    <br><br>
    <input type="submit" value="Sign Up">
  </form>
</body>
</html>