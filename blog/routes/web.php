<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/register', function () {
    return view('register');
});

Route::get('/selamatdatang', function () {
    return view('selamatdatang');
});

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@newwelcome');

Route::get('/master', function() {
    return view('adminlte.master');
});

Route::get('/', function() {
    return view('/items.table');
});

Route::get('/data-tables', function() {
    return view('/items.data');
});